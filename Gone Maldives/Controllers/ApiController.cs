﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Gone_Maldives.DatabaseContext;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Gone_Maldives.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        public ApiController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _context = context;
            webHostEnvironment = hostEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [Route("get")]
        public async Task<IActionResult> Get()
        {
            var _safari = await _context.Safari.FirstOrDefaultAsync();
            return Ok(_safari);
        }

        [HttpPost]
        [Route("delete/{id}")]
        public async Task<IActionResult> delete(string id)
        {
            var _photo = await _context.Images.FindAsync(id);

            if(_photo != null)
            {

                var imagePath = Path.Combine(webHostEnvironment.WebRootPath, "images");
                var imageName = Path.Combine(imagePath, id);

                if (System.IO.File.Exists(imageName))
                {
                    System.IO.File.Delete(imageName);
                }


                _context.Images.Remove(_photo);
                var count = await _context.SaveChangesAsync();
                return Ok(count);
            }else
            {
                return NotFound();
            }
        }
        [HttpGet("setup")]
        public async Task<IActionResult> Setup()
        {
            var user = await _userManager.FindByNameAsync("info@gonemaldives.com");
            if(user == null)
            {
                var result = await _userManager.CreateAsync(new IdentityUser() { UserName = "info@gonemaldives.com", Email = "info@gonemaldives.com" }, "W3lcome!!.!!");
                if(result.Succeeded)
                {
                    return Ok("success");
                }else
                {
                    return Ok("Unable to create user");
                }
            }
            return Ok("");
        }
    }
}