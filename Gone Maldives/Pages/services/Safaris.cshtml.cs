﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Gone_Maldives.Pages.services
{
    public class SafarisModel : PageModel
    {

        private readonly ApplicationDbContext _context;

        public SafarisModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<SafariModel> SafariModel { get; set; }

        public async Task OnGetAsync()
        {
            SafariModel = await _context.Safari.Include(s => s.Images).ToListAsync();
        }
    }
}