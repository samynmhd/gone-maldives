﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Gone_Maldives.Pages.products
{
    public class IndexModel : PageModel
    {


        private readonly ApplicationDbContext _contex;

        public IndexModel(ApplicationDbContext context)
        {
            _contex = context;
        }


        public ResortModel Resort { get; set; }
        [BindProperty]
        public List<ProductViewModel> Product { get; set; }

        public string Title { get; set; }


        public IActionResult OnGet(string title)
        {
            Title = title;

            Product = new List<ProductViewModel>();
            switch (Title)
            {
                case "resorts":
                    var _products = _contex.Resorts.Include(s => s.Images).ToList();
                    foreach(var item in _products)
                    {
                        Product.Add(new ProductViewModel() { Description = item.Description, Id = item.Id, Name = item.Name, Photos = item.Images });
                    }
                    break;
                case "dive-centers":
                    var _dive = _contex.DiveCenters.Include(s => s.Photos).ToList();
                    foreach (var item in _dive)
                    {
                        Product.Add(new ProductViewModel() { Description = item.Description, Id = item.Id, Name = item.Name, Photos = item.Photos });
                    }
                    break;
                case "guest-houses":
                    var _guestHouse = _contex.GuestHouse.Include(s => s.Images).ToList();
                    foreach (var item in _guestHouse)
                    {
                        Product.Add(new ProductViewModel() { Description = item.Description, Id = item.Id, Name = item.Name, Photos = item.Images });
                    }
                    break;
                case "safari":
                    var _safari = _contex.Safari.Include(s => s.Images).ToList();
                    foreach (var item in _safari)
                    {
                        Product.Add(new ProductViewModel() { Description = item.Description, Id = item.Id, Name = item.Name, Photos = item.Images });
                    }
                    break;
                default:
                    return NotFound();
            }

            return Page();
        }
    }
}