﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Gone_Maldives.Pages.services
{
    public class DiveCentersModel : PageModel
    {

        public readonly ApplicationDbContext _context;

        public DiveCentersModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<DiveCenter> DiveCenters { get; set; }

        public async Task OnGetAsync()
        {
            DiveCenters = await _context.DiveCenters.Include(s => s.Photos).ToListAsync();
        }
    }
}