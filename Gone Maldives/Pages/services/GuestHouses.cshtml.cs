﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Gone_Maldives.Pages.services
{
    public class GuestHousesModel : PageModel
    {
        public readonly ApplicationDbContext _context;
        public GuestHousesModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<GuestHouseModel> GuestHouse { get; set; }

        public async Task OnGetAsync()
        {
            GuestHouse = await _context.GuestHouse.Include(s => s.Images).ToListAsync();
        }
    }
}