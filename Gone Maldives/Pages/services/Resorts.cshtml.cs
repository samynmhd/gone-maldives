﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Gone_Maldives.Pages.products
{
    public class ResortsModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public ResortsModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<ResortModel> Resort { get; set; }

        public async Task OnGetAsync()
        {
            Resort = await _context.Resorts.Include(s => s.Images).ToListAsync();
        }
    }
}