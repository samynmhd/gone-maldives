﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using MimeKit;
using MailKit.Net.Smtp;

namespace Gone_Maldives.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }


        public class ContactFormModel
        {
            [Required]
            public string Prefiex { get; set; }
            [Required]
            public string FirstName { get; set; }
           
            [Required]
            public string LastName { get; set; }

            [Required]
            public string Email { get; set; }
           
            [Required]
            public string Address { get; set; }


            public string Address2 { get; set; }

            [Required]
            public string City { get; set; }

            [Required]
            public string State { get; set; }
            
            [Required]
            public string Zipcode { get; set; }

            [Required]
            public string Country { get; set; }

            [Required]
            public string Adults { get; set; }

            [Required]
            public string ChildAgeBelowFive { get; set; }

            [Required]
            public string ChildAgeBelow15 { get; set; }

            [Required]
            public string ArrivalDate { get; set; }

            [Required]
            public string ArrivalMonthYear { get; set; }
        }

        [BindProperty]
        public ContactFormModel Contact { get; set; }

        public void OnGet()
        {

        }


        public async Task<IActionResult> OnPost(ContactFormModel contact)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var mailBody = $@"<p>Hello Gone Maldives,</p>
                    <p>Prefiex: {Contact.Prefiex}</p>
                    <p>Name: {Contact.FirstName} {Contact.LastName}</p>
                    <p>Email: {Contact.Email}</p>
                    <p>Address: {Contact.Address} - {Contact.Address2}</p>
                    <p>City: {Contact.City}</p>
                    <p>State / Province: {Contact.State}</p>
                    <p>Zipcode: {Contact.Zipcode}</p>
                    <p>Country: {Contact.Country}</p>
                    <p>Number of Adults: {Contact.Adults}</p>
                    <p>Children Below 5: {Contact.ChildAgeBelowFive}</p>
                    <p>Children Below 15: {Contact.ChildAgeBelow15}</p>
                    <p>Date of Arrival : {Contact.ArrivalMonthYear}-{Contact.ArrivalDate}</p>

            ";


            SendMail(mailBody);
            return RedirectToPage("Index");
        }


        private void SendMail(string mailBody)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(Contact.FirstName, Contact.Email));
            message.To.Add(new MailboxAddress("Gone Maldives", "info@gonemaldives.com"));
            message.Subject = "New Booking Recieved from " + Contact.FirstName + " "+ Contact.LastName;
            message.Body = new TextPart("html")
            {
                Text = mailBody
            };

            var clientMessage = new MimeMessage();
            clientMessage.From.Add(new MailboxAddress("Gone Maldives", "info@gonemaldives.com"));
            clientMessage.To.Add(new MailboxAddress(Contact.FirstName, Contact.Email));
            clientMessage.Subject = "Recieved Your Booking";
            clientMessage.Body = new TextPart("html")
            {
                Text = $@"<p>Hi {Contact.FirstName} {Contact.LastName},</p>
                       <p>We have recieved your booking, someone will get in touch with you shortly. Thank you for chosing Gone Maldives.</p>
                        <p>Regards</p>              
                "
            };


            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                //client.Connect("smtp.gmail.com", 587, false);
                client.Connect("smtpout.asia.secureserver.net", 465, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                //client.Authenticate("samyn07@gmail.com", "dpxkkpynzaqfdxyb");
                client.Authenticate("info@gonemaldives.com", "hgh##%&234MH");
                client.Send(message);
                client.Send(clientMessage);
                client.Disconnect(true);
            }
        }
    }
}
