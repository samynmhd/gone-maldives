﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace Gone_Maldives.Pages.manage.resorts
{
    public class DeleteModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;
        private readonly IWebHostEnvironment webHostEnvironment;

        public DeleteModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            webHostEnvironment = hostEnvironment;
        }

        [BindProperty]
        public ResortModel ResortModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ResortModel = await _context.Resorts.FirstOrDefaultAsync(m => m.Id == id);

            if (ResortModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ResortModel = await _context.Resorts.Include(s => s.Images).Where(i => i.Id == id).FirstOrDefaultAsync();

            foreach (var photo in ResortModel.Images)
            {

                var imagePath = Path.Combine(webHostEnvironment.WebRootPath, "images");
                var imageName = Path.Combine(imagePath, photo.ImageId);

                if (System.IO.File.Exists(imageName))
                {
                    System.IO.File.Delete(imageName);
                }

                _context.Images.Remove(photo);

            }

            if (ResortModel != null)
            {
                _context.Resorts.Remove(ResortModel);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
