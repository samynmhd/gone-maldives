﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;

namespace Gone_Maldives.Pages.manage.resorts
{
    public class DetailsModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;

        public DetailsModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context)
        {
            _context = context;
        }

        public ResortModel ResortModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ResortModel = await _context.Resorts.Include(s => s.Images).FirstOrDefaultAsync(m => m.Id == id);

            if (ResortModel == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
