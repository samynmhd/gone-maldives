﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;

namespace Gone_Maldives.Pages.manage.safari
{
    public class DetailsModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;

        public DetailsModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context)
        {
            _context = context;
        }

        public SafariModel SafariModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SafariModel = await _context.Safari.Include(s => s.Images).FirstOrDefaultAsync(m => m.Id == id);

            if (SafariModel == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
