﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Gone_Maldives.Repository;
using Microsoft.AspNetCore.Http;

namespace Gone_Maldives.Pages.manage.safari
{
    public class EditModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;
        private IImageRepository _imageRepo;

        public EditModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IImageRepository imageRepo)
        {
            _context = context;
            _imageRepo = imageRepo;
        }

        [BindProperty]
        public SafariModel SafariModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SafariModel = await _context.Safari.Include(s =>s.Images).FirstOrDefaultAsync(m => m.Id == id);

            if (SafariModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(List<IFormFile> files)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(SafariModel).State = EntityState.Modified;
            var model = await _context.Safari.Include(s => s.Images).FirstOrDefaultAsync(m => m.Id == SafariModel.Id);

            long size = files.Sum(f => f.Length);
            if (size > 0)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        string fileName = await _imageRepo.AddImage(file);
                        model.Images.Add(new Image() { ImageId = fileName.ToString() });

                    }
                    await _context.SaveChangesAsync();
                }
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SafariModelExists(SafariModel.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool SafariModelExists(int id)
        {
            return _context.Safari.Any(e => e.Id == id);
        }
    }
}
