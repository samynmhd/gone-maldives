﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Gone_Maldives.Pages.manage.safari
{
    public class DeleteModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;
        private readonly IWebHostEnvironment webHostEnvironment;

        public DeleteModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            webHostEnvironment = hostEnvironment;
        }

        [BindProperty]
        public SafariModel SafariModel { get; set; }
        [BindProperty]
        public Image Image { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SafariModel = await _context.Safari.FirstOrDefaultAsync(m => m.Id == id);

            if (SafariModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SafariModel = await _context.Safari.Include(s => s.Images).Where(s => s.Id == id).FirstOrDefaultAsync();

            foreach (var photo in SafariModel.Images)
            {

                var imagePath = Path.Combine(webHostEnvironment.WebRootPath, "images");
                var imageName = Path.Combine(imagePath, photo.ImageId);

                if (System.IO.File.Exists(imageName))
                {
                    System.IO.File.Delete(imageName);
                }

                _context.Images.Remove(photo);

            }

            if (SafariModel != null)
            {
                _context.Safari.Remove(SafariModel);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
