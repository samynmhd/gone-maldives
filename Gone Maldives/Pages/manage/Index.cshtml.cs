﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;


namespace Gone_Maldives.Pages.manage
{
    public class IndexModel : PageModel
    {

        private ApplicationDbContext db;

        [BindProperty]
        public DiveCenter Dive { get; set; }
        [BindProperty]
        public Image Image { get; set; }
        [BindProperty]
        public PathOptions PathOptions { get; set; }

        public IndexModel(ApplicationDbContext dataContext)
        {
            this.db = dataContext;
        }
            
        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost(List<IFormFile> files)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            var dive = new DiveCenter
            {
                Name = Dive.Name,
                Description = Dive.Description
            };
            this.db.DiveCenters.Add(dive);
            var count  = await this.db.SaveChangesAsync();
            var _dive = await this.db.DiveCenters.Include(s => s.Photos).OrderByDescending(id => id.Id).FirstOrDefaultAsync();
            long size = files.Sum(f => f.Length);
            if(count > 0)
            {
                if(_dive != null)
                {
                    foreach(var formFile in files)
                    {
                        if(formFile.Length > 0)
                        {
                            var filename = Guid.NewGuid();
                            var filePath = Path.Combine(@"wwwroot\Images",filename.ToString());
                            using (var stream = System.IO.File.Create(filePath))
                            {
                                await formFile.CopyToAsync(stream);
                            }
                            _dive.Photos.Add(new Image() { ImageId = filename.ToString() });
                        }
                        this.db.SaveChanges();
                    }

                } else
                {
                    var _dive_ = this.db.DiveCenters.Include(s => s.Photos).FirstOrDefault();


                    foreach (var formFile in files)
                    {
                        if (formFile.Length > 0)
                        {
                            var filename = Guid.NewGuid();
                            var filePath = Path.Combine(@"wwwroot\Images", filename.ToString());
                            using (var stream = System.IO.File.Create(filePath))
                            {
                                await formFile.CopyToAsync(stream);
                            }
                            _dive_.Photos.Add(new Image() { ImageId = filename.ToString() });
                        }
                        this.db.SaveChanges();
                    }
                }

            }



            return RedirectToPage("index");

        }

    }
}