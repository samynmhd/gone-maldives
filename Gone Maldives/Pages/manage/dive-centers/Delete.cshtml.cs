﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Gone_Maldives.Pages.manage.Add
{
    public class DeleteModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;
        private readonly IWebHostEnvironment webHostEnvironment;

        public DeleteModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            webHostEnvironment = hostEnvironment;
        }

        [BindProperty]
        public DiveCenter DiveCenter { get; set; }
        [BindProperty]
        public Image Image { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            DiveCenter = await _context.DiveCenters.FirstOrDefaultAsync(m => m.Id == id);

            if (DiveCenter == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            DiveCenter = await _context.DiveCenters.Include(s => s.Photos).Where(i => i.Id == id).FirstOrDefaultAsync();
            //Image = await _context.Images.FindAsync(Image.ImageId == id.ToString());

            foreach(var photo in DiveCenter.Photos)
            {

                var imagePath = Path.Combine(webHostEnvironment.WebRootPath, "images");
                var imageName = Path.Combine(imagePath, photo.ImageId);

                if (System.IO.File.Exists(imageName))
                {
                    System.IO.File.Delete(imageName);
                }

                _context.Images.Remove(photo);

            }

            if (DiveCenter != null)
            {
                _context.DiveCenters.Remove(DiveCenter);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
