﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;

namespace Gone_Maldives.Pages.manage.Add
{
    public class IndexModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;

        public IndexModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<DiveCenter> DiveCenter { get;set; }

        public async Task OnGetAsync()
        {
            DiveCenter = await _context.DiveCenters.Include(p => p.Photos).ToListAsync();
        }
    }
}
