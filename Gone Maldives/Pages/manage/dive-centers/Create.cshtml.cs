﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Gone_Maldives.Repository;

namespace Gone_Maldives.Pages.manage.Add
{
    public class CreateModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;


        private readonly IConfiguration Configuration;
        private readonly IWebHostEnvironment webHostEnvironment;
        private IImageRepository _imageRepo;

        public CreateModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IConfiguration configuration, IWebHostEnvironment hostEnvironment, IImageRepository imageRepo)
        {
            _context = context;
            Configuration = configuration;
            webHostEnvironment = hostEnvironment;
            _imageRepo = imageRepo;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public PathOptions PathOptions { get; set; }

        [BindProperty]
        public DiveCenter DiveCenter { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(List<IFormFile> files)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.DiveCenters.Add(DiveCenter);
            var count = await _context.SaveChangesAsync();

            var _dive = await _context.DiveCenters.Include(s => s.Photos).OrderByDescending(id => id.Id).FirstOrDefaultAsync();
            long size = files.Sum(f => f.Length);
            if( count > 0)
            {
                if(_dive != null)
                {
                    foreach(var formFile in files)
                    {
                        if(formFile.Length > 0)
                        {
                            var uniqueFileName = await _imageRepo.AddImage(formFile);
                            _dive.Photos.Add(new Image() { ImageId = uniqueFileName.ToString() });
                        }
                       await _context.SaveChangesAsync();
                    }
                } else
                {
                    var _dive_ = await _context.DiveCenters.Include(s => s.Photos).FirstOrDefaultAsync();
                    foreach(var formFile in files)
                    {
                        if(formFile.Length > 0)
                        {
                            var uniqueFileName = await _imageRepo.AddImage(formFile);
                            _dive_.Photos.Add(new Image() { ImageId = uniqueFileName});
                        }
                        await _context.SaveChangesAsync();
                    }
                }
            }

            return RedirectToPage("./Index");
        }
    }
}
