﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Http;
using Gone_Maldives.Repository;

namespace Gone_Maldives.Pages.manage.Add
{
    public class EditModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;
        private IImageRepository _imageRepo;

        public EditModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IImageRepository imageRepo)
        {
            _context = context;
            _imageRepo = imageRepo;
        }

        [BindProperty]
        public DiveCenter DiveCenter { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            DiveCenter = await _context.DiveCenters.Include(s => s.Photos).FirstOrDefaultAsync(m => m.Id == id);

            if (DiveCenter == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(List<IFormFile> files)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(DiveCenter).State = EntityState.Modified;
            var dive = await _context.DiveCenters.Include(s => s.Photos).FirstOrDefaultAsync(m => m.Id == DiveCenter.Id);

            long size = files.Sum(f => f.Length);
            if(size > 0)
            {
                foreach(var file in files)
                {
                    if(file.Length > 0)
                    {
                        string fileName = await _imageRepo.AddImage(file);

                        dive.Photos.Add(new Image() { ImageId = fileName.ToString() });

                    }
                    await _context.SaveChangesAsync();
                }
            }



            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiveCenterExists(DiveCenter.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool DiveCenterExists(int id)
        {
            return _context.DiveCenters.Any(e => e.Id == id);
        }
    }
}
