﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;

namespace Gone_Maldives.Pages.manage.guest_houses
{
    public class IndexModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;

        public IndexModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<GuestHouseModel> GuestHouseModel { get;set; }

        public async Task OnGetAsync()
        {
            GuestHouseModel = await _context.GuestHouse.ToListAsync();
        }
    }
}
