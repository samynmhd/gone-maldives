﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;

namespace Gone_Maldives.Pages.manage.guest_houses
{
    public class DetailsModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;

        public DetailsModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context)
        {
            _context = context;
        }

        public GuestHouseModel GuestHouseModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GuestHouseModel = await _context.GuestHouse.Include(s => s.Images).FirstOrDefaultAsync(m => m.Id == id);

            if (GuestHouseModel == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
