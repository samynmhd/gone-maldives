﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Gone_Maldives.DatabaseContext;
using Gone_Maldives.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.IO;
using Gone_Maldives.Repository;

namespace Gone_Maldives.Pages.manage.guest_houses
{
    public class CreateModel : PageModel
    {
        private readonly Gone_Maldives.DatabaseContext.ApplicationDbContext _context;
        private readonly IWebHostEnvironment webHostEnvironment;
        private IImageRepository _imageRepo;
        public CreateModel(Gone_Maldives.DatabaseContext.ApplicationDbContext context, IWebHostEnvironment hostEnvironment, IImageRepository image)
        {
            _context = context;
            webHostEnvironment = hostEnvironment;
            _imageRepo = image;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public GuestHouseModel GuestHouseModel { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(List<IFormFile> files)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.GuestHouse.Add(GuestHouseModel);
            var count = await _context.SaveChangesAsync();

            var _guestHouse = await _context.GuestHouse.Include(s => s.Images).OrderByDescending(id => id.Id).FirstOrDefaultAsync();
            long size = files.Sum(f => f.Length);
            if (count > 0)
            {
                if (_guestHouse != null)
                {
                    foreach (var formFile in files)
                    {
                        if (formFile.Length > 0)
                        {

                            var uniqueFileName = await _imageRepo.AddImage(formFile);
                            _guestHouse.Images.Add(new Image() { ImageId = uniqueFileName });
                        }
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    var _guestHouse_ = await _context.GuestHouse.Include(s => s.Images).FirstOrDefaultAsync();
                    foreach (var formFile in files)
                    {
                        if (formFile.Length > 0)
                        {
                            var uniqueFileName = await _imageRepo.AddImage(formFile);
                            _guestHouse_.Images.Add(new Image() { ImageId = uniqueFileName });
                        }
                        await _context.SaveChangesAsync();
                    }
                }
            }

            return RedirectToPage("./Index");
        }
    }
}
