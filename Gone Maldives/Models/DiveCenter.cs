﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gone_Maldives.Models
{
    public class DiveCenter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Image> Photos { get; set; }
        public string Description { get; set; }
    }
}
