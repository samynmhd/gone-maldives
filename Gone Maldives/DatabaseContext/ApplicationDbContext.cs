﻿using Gone_Maldives.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gone_Maldives.DatabaseContext
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        { }

        public DbSet<DiveCenter> DiveCenters { get; set; }
        public DbSet<GuestHouseModel> GuestHouse { get; set; }
        public DbSet<ResortModel> Resorts { get; set; }
        public DbSet<SafariModel> Safari { get; set; }
        public DbSet<Image> Images { get; set; }
    }
}
