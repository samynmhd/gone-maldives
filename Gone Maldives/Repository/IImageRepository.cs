﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Gone_Maldives.Repository
{
    public interface IImageRepository
    {
        /// <summary>
        /// Adding Image to the server
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<string> AddImage(IFormFile data);

        /// <summary>
        /// Convert Image to webp and upload to server
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<string> WebPImage(IFormFile data);


    }
}
