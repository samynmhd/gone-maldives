﻿using ImageProcessor;
using ImageProcessor.Plugins.WebP.Imaging.Formats;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Gone_Maldives.Repository
{
    public class ImageRepository : IImageRepository
    {
        private readonly IWebHostEnvironment webHostEnvironment;

        public ImageRepository(IWebHostEnvironment hostEnvironment)
        {
            webHostEnvironment = hostEnvironment;
        }


        public async Task<string> AddImage(IFormFile data)
        {
            string uploadFolder = Path.Combine(webHostEnvironment.WebRootPath, "Images");
            string uniqueFileName = Guid.NewGuid().ToString() + "_" + Path.GetExtension(data.FileName);
            string filePath = Path.Combine(uploadFolder, uniqueFileName);
            using (var filStream = new FileStream(filePath, FileMode.Create))
            {
                await data.CopyToAsync(filStream);
            }

            return uniqueFileName.ToString();
        }

        /// <summary>
        /// Save Images in webp format
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<string> WebPImage(IFormFile data)
        {
            string guid = Guid.NewGuid().ToString();
            string imagesPath = Path.Combine(webHostEnvironment.WebRootPath, "Images");
            string webPFileName = guid + ".webp";
            string webPImagePath = Path.Combine(imagesPath, webPFileName);

            using (var webPFileStream = new FileStream(webPImagePath, FileMode.Create))
            {
                using (ImageFactory imageFactory = new ImageFactory(preserveExifData: false))
                {
                    imageFactory.Load(data.OpenReadStream())
                        .Format(new WebPFormat())
                        .Quality(100)
                        .Save(webPFileStream);
                }
            }

            return webPFileName.ToString();
        }

    }
}