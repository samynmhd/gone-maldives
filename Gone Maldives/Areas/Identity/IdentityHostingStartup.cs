﻿using System;
using Gone_Maldives.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(Gone_Maldives.Areas.Identity.IdentityHostingStartup))]
namespace Gone_Maldives.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<Gone_MaldivesContext>(options =>
                    options.UseSqlite(
                        context.Configuration.GetConnectionString("Gone_MaldivesContextConnection")));

                services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = false)
                    .AddEntityFrameworkStores<Gone_MaldivesContext>();
            });
        }
    }
}